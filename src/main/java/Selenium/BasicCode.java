package Selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BasicCode {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Jars\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		System.out.println("User Opens new Chrome browser");
		
		driver.get("https://www.facebook.com");
		System.out.println("User enters URL");
		System.out.println("User wants to close the browser");
		driver.close();
		System.out.println("Driver closed");
	}

}
