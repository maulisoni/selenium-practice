package Selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CustomXpath {

	public static void main(String[] args) 
	{
		String URL = "https://app.hubspot.com/login";
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(URL);
		System.out.println("User Opens the URL");
		
		//Create an account in Hubspot.com
		String SignupLink = "//i18n-string[text()='Sign up']";
		driver.findElement(By.xpath(SignupLink)).click();
		System.out.println("Sign-up link clicked successfully");
		
		String firstName = "//input[starts-with(@id,'uid-firstName')]";
		driver.findElement(By.xpath(firstName)).sendKeys("Mauli");
		String lastName = "//input[starts-with(@id,'uid-lastName')]";
		driver.findElement(By.xpath(lastName)).sendKeys("Soni");
		
		String emailId = "(//input[contains(@class,'private-form__control')])[3]";
		driver.findElement(By.xpath(emailId)).sendKeys("maulisoni30@gmail.com");
		
		String nextBtn = "//button[contains(@class,'private-button--non-link') and @type='submit']";
		driver.findElement(By.xpath(nextBtn)).click();
		System.out.println("User receives an email to verify account");
	
		driver.close();
		System.out.println("close browser");
	}

}
