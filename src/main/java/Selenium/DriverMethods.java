package Selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverMethods {

	public static void main(String[] args) throws Exception 
	{
		WebDriverManager.chromedriver().setup();
		WebDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://app.hubspot.com");
		Thread.sleep(20000);
		driver.manage().timeouts().pageLoadTimeout(2, TimeUnit.MINUTES);
		System.out.println("Äpplication opens");
		//driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
		driver.quit();
		System.out.println("Driver quitted");
		driver= new ChromeDriver();
		System.out.println("Open new chrome browser after quitting");
		driver.getTitle();
		driver.close();
		System.out.println("Driver closed");
		driver=new ChromeDriver();
		System.out.println("Open new chrome browser after closing");
		driver.get("https://app.hubspot.com");
		driver.getTitle();
		driver.close();
	}

}
